#!/bin/bash
basename=${1##*/}
expand --tabs=4 $1 > /tmp/$basename
cp /tmp/$basename $1
rm /tmp/$basename
