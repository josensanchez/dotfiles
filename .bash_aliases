# aliases

alias st='git status'

alias llog='tail -f ~/projects/stensul/sitesV3/base/storage/logs/laravel-`date +%Y-%m-%d`.log'
alias modu='cd ~/projects/mediasan/modules'
alias docks='cd ~/projects/stensul/docker-testing'
alias secre='cd ~/projects/mediasan/reactsecretaria'
alias prov='cd ~/projects/die/prov'
alias mn='cd ~/projects/die/managernacional'

alias dcu='docker-compose up'
alias dcrm='docker-compose rm -f'
alias dcst='docker stop $(docker ps -q)'
alias mediasan='cd ~/projects/mediasan'
alias die='cd ~/projects/die'
alias frontid='cd ~/projects/frontid'
alias coit='cd ~/projects/frontid/coit/web'
alias siasar='cd ~/projects/frontid/siasar/web'
alias cepal='cd ~/projects/cepal/comtrade-ETL/flask'

alias inertia='cd ~/projects/stensul/extras/inertia-app'
alias vite='cd ~/projects/stensul/extras/my-vite-app'
alias livewire='cd ~/projects/stensul/extras/livewire-app'

alias sail='bash vendor/bin/sail'

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
export PATH="$HOME/.npm-global/bin:$HOME/bin:$PATH"


fixInterfaceScope() {
    interface=$1
    data=($(ip -o address show "$interface" | awk -F ' +' '{print $4 " " $6 " " $8}'))
    
    LOCAL_ADDRESS_INDEX=0
    PEER_ADDRESS_INDEX=1
    SCOPE_INDEX=2
    
    if [ "${data[$SCOPE_INDEX]}" == "global" ]
    then
        echo "Interface ${interface} is already set to global scope. Skip!"
        return
    else
        echo "Interface ${interface} is set to scope ${data[$SCOPE_INDEX]}."
        tmpfile=$(mktemp --suffix=snxwrapper-routes)
        echo "Saving current IP routing table..."
        sudo ip route save > $tmpfile
        echo "Deleting current interface ${interface}..." 
        sudo ip address del ${data[$LOCAL_ADDRESS_INDEX]} peer ${data[$PEER_ADDRESS_INDEX]} dev ${interface}
        echo "Recreating interface ${interface} with global scope..." 
        sudo ip address add ${data[$LOCAL_ADDRESS_INDEX]} dev ${interface} peer ${data[$PEER_ADDRESS_INDEX]} scope global
        echo "Restoring routing table..."
        sudo ip route restore < $tmpfile 2>/dev/null
        echo "Cleaning temporary files..."
        rm $tmpfile
        echo "Interface ${interface} is set to global scope. Done!"
    fi
}
