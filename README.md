# dotfiles

comes from https://www.atlassian.com/git/tutorials/dotfiles


##Restore in a new system

append into your .bashrc (or .zsh or whatever you use)

```bash
echo "alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'" >> .bashrc
```

clone bare repository

```bash
git clone --bare <git-repo-url> $HOME/.cfg
```

make sure you have .cfg in your .gitignore

```
echo ".cfg" >> .gitignore
```

I personally run `bash` to update the config but you can do as you please here, if nothing works you can close and open a new terminal

then, pull the changes into the working directory

```
config checkout
```

The step above might fail with a message like:

```
error: The following untracked working tree files would be overwritten by checkout:
    .bashrc
    .gitignore
Please move or remove them before you can switch branches.
Aborting
```

This is because your $HOME folder might already have some stock configuration files which would be overwritten by Git. The solution is simple: back up the files if you care about them, remove them if you don't care. I provide you with a possible rough shortcut to move all the offending files automatically to a backup folder:

in that case, follow the instrucions in https://www.atlassian.com/git/tutorials/dotfiles
